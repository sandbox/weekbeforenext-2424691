<?php
/**
 * Override views_data().
 */
function webform_report_views_views_data() {

  $data['webform_report']['table']['group'] = t('Webform Report');
  $data['webform_report']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  $data['webform_report']['nid'] = array(
    'title' => t('Webform Report NID'),
    'help' => t('The nid for the webform report node.'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Referenced Webform Report NID'),
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
      'name field' => 'name',
      'numeric' => TRUE,
      'validate type' => 'nid'
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric'
    ),
    'sort' => array(
      'handler' => 'views_handler_sort'
    ),
  );

  $data['webform_report']['wnid'] = array(
    'title' => t('Webform NID'),
    'help' => t('The nid for the webform node.'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'wnid',
      'handler' => 'views_handler_relationship',
      'label' => t('Referenced Webform NID'),
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
      'name field' => 'name',
      'numeric' => TRUE,
      'validate type' => 'wnid'
    ),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is use by the table display plugin.
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric'
    ),
    'sort' => array(
      'handler' => 'views_handler_sort'
    ),
  );

  $data['webform_report']['description'] = array(
    'title' => t('Webform Report Description'),
    'help' => t('The description for the webform report.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is use by the table display plugin.
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  return $data;

}